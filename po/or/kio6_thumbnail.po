# translation of kio_thumbnail.po to Oriya
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Manoj Kumar Giri <mgiri@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kio_thumbnail\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-02-25 00:40+0000\n"
"PO-Revision-Date: 2009-01-02 11:48+0530\n"
"Last-Translator: Manoj Kumar Giri <mgiri@redhat.com>\n"
"Language-Team: Oriya <oriya-it@googlegroups.com>\n"
"Language: or\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"

#: thumbnail.cpp:205
#, kde-format
msgid "No MIME Type specified."
msgstr ""

#: thumbnail.cpp:212
#, kde-format
msgid "No or invalid size specified."
msgstr ""

#: thumbnail.cpp:233
#, kde-format
msgid "Cannot create thumbnail for directory"
msgstr ""

#: thumbnail.cpp:242
#, kde-format
msgid "No plugin specified."
msgstr ""

#: thumbnail.cpp:247
#, kde-format
msgid "Cannot load ThumbCreator %1"
msgstr ""

#: thumbnail.cpp:255
#, kde-format
msgid "Cannot create thumbnail for %1"
msgstr ""

#: thumbnail.cpp:265
#, kde-format
msgid "Failed to create a thumbnail."
msgstr ""

#: thumbnail.cpp:285
#, kde-format
msgid "Could not write image."
msgstr ""

#: thumbnail.cpp:313
#, kde-format
msgid "Failed to attach to shared memory segment %1"
msgstr ""

#: thumbnail.cpp:317
#, kde-format
msgid "Image is too big for the shared memory segment"
msgstr ""
